//SPLIT-EXPENSE STUDENTS BILLS CALCULATOR v1.0

let students = document.querySelectorAll('.students');

const billInputCheck = document.querySelector('.billInputCheck');

const printPage = document.querySelector('.printPage');
const start = document.querySelector('.start');
const erase = document.querySelector('.erase');
const readInfo = document.querySelector('.readInfo');

const log = document.querySelector('#log');

billInputCheck.addEventListener('change' , () => {                                      //This event listener engages the function
    let billAmount = Number(document.querySelector('#billAmount').value);               //to check if billAmount value is not a negative number.
    if(billAmount <= 0){
        document.querySelector('#billAmount').value = "";
        let div = document.createElement('div');
        log.innerHTML = "";
        log.append(div);
        div.innerHTML = "L'importo della fattura non può essere negativo o nullo.";
    } else {
        log.innerHTML = "";
    }
})

students.forEach(student => {                                                           //This event listener engages the function
    const daysInputCheck = student.querySelector('.days');                              //to check if studentDays value is not a negative or float number.
    daysInputCheck.addEventListener('change' , () => {
        let studentDays = Number(student.querySelector('.days').value);
        if((studentDays <= 0) || !(Number.isInteger(studentDays))){
            student.querySelector('.days').value = "";
            let div = document.createElement('div');
            log.innerHTML = "";
            log.append(div);
            div.innerHTML = "Il numero di giorni di presenza dello studente non può essere negativo, nullo o con la virgola.";
        } else {
            log.innerHTML = "";
        }
    })
})

printPage.addEventListener('click' , () => {                                            //This event listener engages the function
    window.print();                                                                     //for printing page.
})

start.addEventListener('click' , () => {                                                //This event listener engages the function
    let billName = document.querySelector('#billName').value;                           //to load and elaborate data.
    let billAmount = Number(document.querySelector('#billAmount').value);
    let div = document.createElement('div');
    log.innerHTML = "";
    log.append(div);
    let studentsSumDays = 0;
    let total = 0;
    students.forEach(student => {
        const name = student.querySelector('.name').value;
        const studentDays = Number(student.querySelector('.days').value);
        if((name != "") && (studentDays != 0) && (billAmount != 0)){
            studentsSumDays = studentsSumDays + studentDays;
        }
    })
    students.forEach(student => {
        const name = student.querySelector('.name').value;
        const studentDays = Number(student.querySelector('.days').value);
        if((name != "") && (studentDays != 0) && (billAmount != 0)){
            const result = processData(name, billAmount, studentsSumDays, studentDays);
            total = (Number(total) + result).toFixed(2);
        }
    })
    div.innerHTML = "Calcolo per fattura " + billName + " ---------- " + "Somma di verifica: " + total + "€";
})

erase.addEventListener('click' , () => {                                                //This event listener engages the reset function
    reset();                                                                            //to clear all the data fields.
})

readInfo.addEventListener('click', () => {                                              //This event listener engages an alert to show
                                                                                        //a message for the user.
    alert('v1.0\nWritten by Livio Bellini\nOn May 7th 2023\nI wrote this program for my mother so she no longer has to waste time calculating split-expense students bills.\nCiao Mamma!😜');
})

function processData(a, b, c, d){                                                       //This function elaborates the data.
    const result = ((b / c) * d).toFixed(2);
    let div = document.createElement('div');
    log.append(div);
    div.innerHTML = "La spesa di " + a + ": " + result + "€";
    return Number(result);
}

function reset(){                                                                       //This function clears all the data fields.
    log.innerHTML = "";
    document.querySelector('#billName').value = "";
    document.querySelector('#billAmount').value = "";
    students.forEach(student => {
        student.querySelector('.name').value = "";
        student.querySelector('.days').value = "";
    })
}

reset();                                                                                //At the load of the page, reset function is always called.

